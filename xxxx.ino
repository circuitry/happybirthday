#include <Arduino.h>
#include "SPIFFS.h"
#include "AudioFileSourceSPIFFS.h"
#include "AudioGeneratorMP3.h"
#include "AudioOutputI2SNoDAC.h"

#define BLINK(x) { for (int i = 0; i < 10; i++) { digitalWrite(2, LOW); delay(x); digitalWrite(2, HIGH); delay(x); } }

int leds[] = { 15, 2, 4, 5, 18, 19, 21, 23 };
int position = 0;

AudioFileSourceSPIFFS *file;
AudioOutputI2S *dac;
AudioGeneratorMP3 *mp3;

void setup() {
  for (int i = 0; i < sizeof(leds); i++) pinMode(leds[i], OUTPUT);
  SPIFFS.begin();
  file = new AudioFileSourceSPIFFS("/hb.mp3");
  dac = new AudioOutputI2SNoDAC();
  mp3 = new AudioGeneratorMP3();
  mp3->begin(file, dac);
}

void loop() {
  if (mp3->isRunning()) {
    position++;
    int x = (position / 1000) % sizeof(leds);
    for (int i = 0; i < sizeof(leds); i++) digitalWrite(leds[i], x == i);
    if (!mp3->loop()) mp3->stop(); 
  } else {
    Serial.printf("MP3 done\n");
    delay(1000);
  }
}

/*
include <Arduino.h>
include <ESP8266SAM.h>
include <AudioOutputI2SNoDAC.h>

AudioOutputI2S *out = new AudioOutputI2SNoDAC();
ESP8266SAM *sam = new ESP8266SAM;

void setup() {
  out->begin();
  pinMode(2, OUTPUT);
}

void loop() {
  digitalWrite(2, HIGH);
  sam->Say(out, "Happy birthday Andrew!");
  digitalWrite(2, LOW);
  delay(1000);
} 
*/
